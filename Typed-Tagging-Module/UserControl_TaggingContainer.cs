﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace Typed_Tagging_Module
{
    public partial class UserControl_TaggingContainer : UserControl
    {
        private clsLocalConfig objLocalConfig;

        private clsDataWork_Tagging objDataWork_Tagging;

        private OntologyModDBConnector objDBLevel_OItems;

        private clsOntologyItem objOItem_TaggingSource;

        private List<clsTabControl> TabControlList = new List<clsTabControl>();

        private frmMain objFrmOntologyEditor;
        private frm_ObjectEdit objFrmObjectEdit;

        public clsOntologyItem OItem_TaggingSource
        {
            get { return objOItem_TaggingSource; }
        }

        public List<clsOntologyItem> Tags
        {
            get { return objDataWork_Tagging.OList_Tags(); }
        }

        public UserControl_TaggingContainer(clsLocalConfig LocalConfig)
        {
            InitializeComponent();

            objLocalConfig = LocalConfig;
            
            Initialize();
        }

        public UserControl_TaggingContainer(Globals Globals, clsOntologyItem OItem_User, clsOntologyItem OItem_Group)
        {
            InitializeComponent();

            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            objLocalConfig.OItem_User = OItem_User;
            objLocalConfig.OItem_Group = OItem_Group;

            Initialize();
        }

        private void Initialize()
        {

            objDBLevel_OItems = new OntologyModDBConnector(objLocalConfig.Globals);
            objDataWork_Tagging = new clsDataWork_Tagging(objLocalConfig);
        }

        public void Initialize_Taging(clsOntologyItem OItem_TaggingSource, Boolean keepTabs = false)
        {
            objOItem_TaggingSource = OItem_TaggingSource;
            if (objOItem_TaggingSource != null)
            {
                var objOItem_Result = objDataWork_Tagging.GetTagsOfTaggingSource(objOItem_TaggingSource);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    var objOList_Tags = objDataWork_Tagging.OList_Tags();
                    var tabPages = new List<TabPage>();
                    foreach (TabPage tabPage in tabControl1.TabPages)
                    {
                        tabPages.Add(tabPage);

                    }

                    

                    if (!keepTabs)
                    {
                        tabPages.ForEach(tp => Clear_TabPage(tp));
                        TabControlList.Clear();
                    }
                    
                    var objOList_Classes = (from objClass in objOList_Tags
                                            where objClass.Type == objLocalConfig.Globals.Type_Object
                                            group objClass by objClass.GUID_Parent into objClasses
                                            select objDBLevel_OItems.GetOItem(objClasses.Key, objLocalConfig.Globals.Type_Class)).ToList();

                    objOList_Classes.ForEach(cl => Configure_TabPages(cl));

                    Configure_TabPages();
                }
                else
                {
                    MessageBox.Show(this, "Die Tags konnten nicht ermittelt werden!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            
        }
        
        private void Clear_TabPage(TabPage tabPage)
        {
            var controls = TabControlList.Where(ci => ci.TabName == tabPage.Text).ToList();

            if (controls.Any())
            {
                controls.First().objUserControl_Tagging = null;
            }

            tabPage.Controls.Clear();
            tabControl1.TabPages.Remove(tabPage);
            tabPage = null;
        }

        private void Configure_TabPages(clsOntologyItem OItem_Class, bool createUserControl = false)
        {
            var tabPageNew = new TabPage(OItem_Class.Name);


            var tabControl = new clsTabControl
            {
                TabName = OItem_Class.Name,
                ClassItem = OItem_Class//,
                //objUserControl_Tagging = objUserControl
            };
            TabControlList.Add(tabControl);
            tabControl1.TabPages.Add(tabPageNew);

            if (createUserControl)
            {
                var objUserControl = new UserControl_Tagging(objLocalConfig);
                objUserControl.Dock = DockStyle.Fill;
                tabPageNew.Controls.Add(objUserControl);
                tabControl.objUserControl_Tagging = objUserControl;
                tabControl.objUserControl_Tagging.Initialize_Tagging(objOItem_TaggingSource, objDataWork_Tagging, tabControl.ClassItem);
            }
        }

        private void Configure_TabPages()
        {
            if (tabControl1.TabPages.Count > 0)
            {
                var selControls = TabControlList.Where(cl => cl.TabName == tabControl1.SelectedTab.Text).ToList();
                if (selControls.Any())
                {
                    if (selControls.First().objUserControl_Tagging == null)
                    {
                        var objUserControl = new UserControl_Tagging(objLocalConfig);
                        objUserControl.Dock = DockStyle.Fill;
                        tabControl1.SelectedTab.Controls.Add(objUserControl);
                        selControls.First().objUserControl_Tagging = objUserControl;
                    }
                    
                    selControls.First().objUserControl_Tagging.Initialize_Tagging(objOItem_TaggingSource, objDataWork_Tagging, selControls.First().ClassItem);
                }
            }

            
        }

        private void AddObjectTag(string className, clsOntologyItem objectItem)
        {
            if (tabControl1.TabPages.Count > 0)
            {
                var selControl = TabControlList.FirstOrDefault(cl => cl.TabName == className);
                if (selControl != null)
                {
                    if (!selControl.objUserControl_Tagging.TagList.Any(tagSource => tagSource.ID_TaggingDest == objectItem.GUID))
                    {
                        selControl.objUserControl_Tagging.AddNewTag(objectItem);
                    }
                    
                }
                else
                {
                    MessageBox.Show(this, "Das Objekt konnte nicht hinzugefügt werden!", "Klasse nicht gefunden.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Configure_TabPages();
        }

        public void ApplyItems(List<clsOntologyItem> appliedItems)
        {
            var errorItems = new List<clsOntologyItem>();

            var appliedClasses = appliedItems.Where(appliedItem => !string.IsNullOrEmpty(appliedItem.GUID_Parent))
                .GroupBy(appliedItem => appliedItem.GUID_Parent).ToList()
                .Select(classItem => objDBLevel_OItems.GetOItem(classItem.Key, objLocalConfig.Globals.Type_Class)).ToList();

            
            appliedItems.ForEach(appliedItem =>
            {
                
                if (appliedItem.Type == objLocalConfig.Globals.Type_Object)
                {
                    var classItem = appliedClasses.FirstOrDefault(appliedClass => appliedClass.GUID == appliedItem.GUID_Parent);
                    if (classItem != null)
                    {
                        var lTabExist = TabControlList.Where(tb => tb.TabName == classItem.Name);
                        if (!lTabExist.Any())
                        {
                            Configure_TabPages(classItem, true);
                            AddObjectTag(classItem.Name, appliedItem);

                            Configure_TabPages();
                        }
                        else
                        {
                            AddObjectTag(classItem.Name, appliedItem);
                        }
                    }
                    else
                    {
                        errorItems.Add(appliedItem);
                    }
                }
                else
                {
                    errorItems.Add(appliedItem);
                }
            });

            if (errorItems.Any())
            {
                MessageBox.Show(this, errorItems.Count.ToString() + " Items konnten nicht hinzuugefügt werden!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void toolStripButton_AddTypedTag_Click(object sender, EventArgs e)
        {
            objFrmOntologyEditor = new frmMain(objLocalConfig.Globals);
            objFrmOntologyEditor.ShowDialog(this);

            if (objFrmOntologyEditor.DialogResult == DialogResult.OK)
            {
                if (objFrmOntologyEditor.Type_Applied == objLocalConfig.Globals.Type_Class || objFrmOntologyEditor.Type_Applied == objLocalConfig.Globals.Type_Object)
                {
                    var objOList_Classes = objFrmOntologyEditor.OList_Simple;
                    if (objOList_Classes.Count == 1)
                    {
                        clsOntologyItem objOItem_Class = null;
                        var objOItem_Selected = objOList_Classes.First();

                        if (objOItem_Selected.Type == objLocalConfig.Globals.Type_Class)
                        {
                            objOItem_Class = objOItem_Selected;
                        }
                        else if (objOItem_Selected.Type == objLocalConfig.Globals.Type_Object)
                        {
                            objOItem_Class = objDBLevel_OItems.GetOItem(objOItem_Selected.GUID_Parent, objLocalConfig.Globals.Type_Class);
                        }

                        if (objOItem_Class != null)
                        {
                            var lTabExist = TabControlList.Where(tb => tb.TabName == objOItem_Class.Name);
                            if (!lTabExist.Any())
                            {
                                
                                
        
                                if (objOItem_Selected.Type == objLocalConfig.Globals.Type_Object)
                                {
                                    Configure_TabPages(objOItem_Class,true);
                                    AddObjectTag(objOItem_Class.Name, objOItem_Selected);
                                }
                                else
                                {
                                    Configure_TabPages(objOItem_Class);
                                }

                                Configure_TabPages();
                            }
                            else
                            {
                                AddObjectTag(objOItem_Class.Name, objOItem_Selected);
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "Bitte nur eine Klasse oder ein Objekt auswählen!", "Eine Klasse.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        

                        
                    }
                    else
                    {
                        MessageBox.Show(this, "Bitte nur eine Klasse auswählen!", "Eine Klasse.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show(this, "Bitte nur eine Klasse auswählen!", "Eine Klasse.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

    
    }
}
